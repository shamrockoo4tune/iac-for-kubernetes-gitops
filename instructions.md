# Инструкции по развертыванию

## Убедиться что ресурсов не создано
```bash
$ terraform state list
```
Также, проверить в консоли облака  

<br>  

## Создание ресурса с ключами
  
```bash
$ terraform plan -var ceph_count=0
```  

<br>  

## Настройка доступа kubectl в кластер

```bash
$ yc managed-kubernetes cluster list
+---------------+----------------+-----------------+---------+---------+-------------------+--------------------+
|      ID       |      NAME      |   CREATED AT    | HEALTH  | STATUS  | EXTERNAL ENDPOINT | INTERNAL ENDPOINT  |
+---------------+----------------+-----------------+---------+---------+-------------------+--------------------+
| <ID кластера> | <имя кластера> | <дата создания> | HEALTHY | RUNNING | <внешний адрес>   | <внутренний адрес> |
+---------------+----------------+-----------------+---------+---------+-------------------+--------------------+

$ export CLUSTER_ID=<ID кластера>

# Запросить генерацию kubeconfig:
$ yc managed-kubernetes cluster get-credentials --id $CLUSTER_ID --external

$ k config get-clusters 
NAME
yc-managed-k8s-<ID кластера>

# Проверить связанность с api-сервером кластера: 
$ k cluster-info
```  

## Установка ceph-ansible

Включить **nat** на **ceph01-test** для доступа.   
[ceph-ansible](https://github.com/ceph/ceph-ansible.git) будет устанавливаться на него. 

```bash
# Установка pip
$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

$ sudo python get-pip.py

$ pip --version

$ rm get-pip.py


# Пакеты
$ sudo apt-get update

$ sudo apt-get install software-properties-common gnupg2 git

$ sudo -i

# echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" > /etc/apt/sources.list.d/ansible.list 

# exit

$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367

$ sudo apt install ansible 

$ git clone https://github.com/ceph/ceph-ansible.git

$ cd ceph-ansible

$ git checkout stable-7.0

$ pip install -r requirements.txt

$ sudo ansible-galaxy install -r requirements.yml

$ touch ansible.log

$ ls | grep -i ansible 
ansible.cfg
ansible.log      # создать чтобы не ругался
ansible_rsa      # добавить ключи созданные terraform'ом
ansible_rsa.pub  

# Инвентарь
$ cat inventory.ini 
ceph01-test ansible_host=10.10.0.201 
ceph02-test ansible_host=10.10.0.202 
ceph03-test ansible_host=10.10.0.203

[all:vars]
ansible_connection=ssh
ansible_user=ansible
ansible_python_interpreter=/usr/bin/python3
ansible_ssh_common_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
ansible_ssh_private_key_file=/home/ansible/ceph-ansible/ansible_rsa

[mons]
ceph01-test
ceph02-test
ceph03-test

[osds]
ceph01-test
ceph02-test
ceph03-test
    
$ ansible -m ping -i ./inventory.ini all -l ceph01-test

# Копируем и наполняем переменные для установки кластера
$ cp ./group_vars/all.yml.sample ./group_vars/all.yml

$ cat ./group_vars/all.yml | grep -Ev "^#|^$"
---
dummy:
ceph_release_num:
  quincy: 17
cluster: ceph
mon_group_name: mons
osd_group_name: osds
ntp_service_enabled: false
ceph_origin: repository
ceph_repository: community
ceph_stable_release: quincy
fsid: "{{ cluster_uuid.stdout }}"
generate_fsid: true
monitor_interface: eth0
ip_version: ipv4
journal_size: 5120 # OSD journal size in MB
public_network: 10.10.0.0/24
cluster_network: "{{ public_network | regex_replace(' ', '') }}"
ceph_conf_overrides:
  global:
    osd_pool_default_pg_num: 64
    osd_journal_size: 5120
    osd_pool_default_size: 3
    osd_pool_default_min_size:  2
dashboard_enabled: False

# /home/ansible/ceph-ansible/group_vars/osds.yml
$ cat ./group_vars/osds.yml | grep -E -v '^#|^$'
---
dummy:
  - /dev/vdb
  - /dev/vdc
  - /dev/vdd
osds_per_device: 1

# Прописать хосты на всех нодах
$ cat /etc/hosts | grep ^10.
10.10.0.201 ceph01-test 
10.10.0.202 ceph02-test  
10.10.0.203 ceph03-test 

# отключить косячную проверку  The conditional check 'item.disk.table == 'gpt'' failed. The error was: error while evaluating conditional (item.disk.table == 'gpt'): 'dict object' has no attribute 'disk'

$ cat ./roles/ceph-validate/tasks/check_devices.yml -n 
...

    49	#    - name: fail when gpt header found on osd devices
    50	#      fail:
    51	#        msg: "{{ item.disk.dev }} has gpt header, please remove it."
    52	#      with_items: "{{ devices_parted.results }}"
    53	#      when:
    54	#        - item.skipped is undefined
    55	#        - item.disk.table == 'gpt'
    56	#        - item.partitions | length == 0
    57	
...

sudo /usr/bin/python3.9 -m pip install netaddr

# Раскатать кластер
sudo ansible-playbook -i inventory.ini ./site.yml
```  

Примеры подготовленных файлов:  
* [all.yml](./ansible/examples/all.yml)
* [inventory.ini](./ansible/examples/inventory.ini)
* [osds.yml](./ansible/examples/osds.yml)
* [site.yml](./ansible/examples/site.yml)

<br>  

## Работа с кластером

```bash
$ sudo -i
root@ceph01-test:~# ceph status
  cluster:
    id:     <выводит ID кластера>
    health: HEALTH_WARN
            mons are allowing insecure global_id reclaim
 
  services:
    mon: 3 daemons, quorum ceph01-test,ceph02-test,ceph03-test (age 6m)
    mgr: ceph03-test(active, since 6m), standbys: ceph02-test, ceph01-test
    osd: 9 osds: 9 up (since 5m), 9 in (since 5m)
 
  data:
    pools:   1 pools, 1 pgs
    objects: 2 objects, 449 KiB
    usage:   981 MiB used, 179 GiB / 180 GiB avail
    pgs:     1 active+clean


# ceph config set mon auth_allow_insecure_global_id_reclaim false

# ceph status
  cluster:
    id:     <выводит ID кластера>
    health: HEALTH_WARN
            1 daemons have recently crashed
 
  services:
    mon: 3 daemons, quorum ceph01-test,ceph02-test,ceph03-test (age 11m)
    mgr: ceph03-test(active, since 10m), standbys: ceph02-test, ceph01-test
    osd: 9 osds: 9 up (since 9m), 9 in (since 9m)
 
  data:
    pools:   1 pools, 1 pgs
    objects: 2 objects, 449 KiB
    usage:   981 MiB used, 179 GiB / 180 GiB avail
    pgs:     1 active+clean
 
root@ceph01-test:~# ceph crash ls
ID                                                                ENTITY           NEW  
2023-09-24T17:03:50.100071Z_187592a7-2bfb-4fa1-bd2a-952b2ec64cd5  mon.ceph03-test   *   

# ceph crash archive-all

# ceph status
  cluster:
    id:     <выводит ID кластера>
    health: HEALTH_OK
 
  services:
    mon: 3 daemons, quorum ceph01-test,ceph02-test,ceph03-test (age 13m)
    mgr: ceph03-test(active, since 12m), standbys: ceph02-test, ceph01-test
    osd: 9 osds: 9 up (since 11m), 9 in (since 11m)
 
  data:
    pools:   1 pools, 1 pgs
    objects: 2 objects, 449 KiB
    usage:   981 MiB used, 179 GiB / 180 GiB avail
    pgs:     1 active+clean

root@ceph01-test:~# ceph -s
  cluster:
    id:     <выводит ID кластера>
    health: HEALTH_OK
 
  services:
    mon: 3 daemons, quorum ceph01-test,ceph02-test,ceph03-test (age 14m)
    mgr: ceph03-test(active, since 13m), standbys: ceph02-test, ceph01-test
    osd: 9 osds: 9 up (since 12m), 9 in (since 12m)
 
  data:
    pools:   1 pools, 1 pgs
    objects: 2 objects, 449 KiB
    usage:   981 MiB used, 179 GiB / 180 GiB avail
    pgs:     1 active+clean
 
root@ceph01-test:~# rados lspools
.mgr
root@ceph01-test:~# ceph df
--- RAW STORAGE ---
CLASS     SIZE    AVAIL     USED  RAW USED  %RAW USED
hdd    180 GiB  179 GiB  981 MiB   981 MiB       0.53
TOTAL  180 GiB  179 GiB  981 MiB   981 MiB       0.53
 
--- POOLS ---
POOL  ID  PGS   STORED  OBJECTS     USED  %USED  MAX AVAIL
.mgr   1    1  449 KiB        2  449 KiB      0     56 GiB
root@ceph01-test:~# 
```  

<br>  

## Выключить кластер

На мэнеджер стендбай нодах:
```bash
root@ceph01-test:~# ceph osd set noout
root@ceph01-test:~# ceph osd set nobackfill
root@ceph01-test:~# ceph osd set norecover
root@ceph01-test:~# ceph osd set norebalance
root@ceph01-test:~# ceph osd set nodown
root@ceph01-test:~# ceph osd set pause 
```

Затем на активном мэнеджере:
```bash
root@ceph03-test:~# ceph osd set noout
root@ceph03-test:~# ceph osd set nobackfill
root@ceph03-test:~# ceph osd set norecover
root@ceph03-test:~# ceph osd set norebalance
root@ceph03-test:~# ceph osd set nodown
root@ceph03-test:~# ceph osd set pause 
```

Затем откоючить ноды  

<br>  

## Включить ранее отключенный кластер

Включить все ноды, дождаться загрузки и сетевой связностию Выполнить:
```bash
$ sudo -i
root@ceph03-test:~# ceph osd unset noout
root@ceph03-test:~# ceph osd unset nobackfill
root@ceph03-test:~# ceph osd unset norecover
root@ceph03-test:~# ceph osd unset norebalance
root@ceph03-test:~# ceph osd unset nodown
root@ceph03-test:~# ceph osd unset pause 
root@ceph03-test:~# ceph -s 
```  

<br>  

## Подключение к k8s

Создаем пул, клиента с ключем
```bash
root@ceph01-test:~# ceph osd pool create kube 8 8

root@ceph01-test:~# ceph auth add client.kube mon 'allow r' osd 'allow rwx pool=kube'
added key for client.kube

root@ceph01-test:~# ceph auth get client.kube
[client.kube]
        key = <kube key>
        caps mon = "allow r"
        caps osd = "allow rwx pool=kube"

root@ceph01-test:~# ceph auth get client.admin
[client.admin]
        key = <admin key>
        caps mds = "allow *"
        caps mgr = "allow *"
        caps mon = "allow *"
        caps osd = "allow *"
```

Выведем информацию по кластеру:
```bash
ceph> mon dump
dumped monmap epoch 1
epoch 1
fsid <fsid>
last_changed 2023-09-24T17:03:42.047135+0000
created 2023-09-24T17:03:42.047135+0000
min_mon_release 17 (quincy)
election_strategy: 1
0: [v2:10.10.0.201:3300/0,v1:10.10.0.201:6789/0] mon.ceph01-test
1: [v2:10.10.0.202:3300/0,v1:10.10.0.202:6789/0] mon.ceph02-test
2: [v2:10.10.0.203:3300/0,v1:10.10.0.203:6789/0] mon.ceph03-test
```

Далее операции в кластере k8s

Устанавливаем CSI driver
helm repo add ceph-csi https://ceph.github.io/csi-charts

helm inspect values ceph-csi/ceph-csi-cephfs > ceph-csi/cephfs.yml

заполняем cephfs.yml

устанавливаем чарт

$ helm upgrade -i ceph-csi-cephfs ceph-csi/ceph-csi-cephfs -f ceph-csi/cephfs.yml -n ceph-csi-cephfs --create-namespace

Создаем secret

Получаем admin key:
root@ceph01-test:~# ceph auth get-key client.admin
<выводит admin key> 

Создаем [манифест с секретом](./ceph-csi/secret.yaml)  

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: csi-cephfs-secret
  namespace: ceph-csi-cephfs
stringData:
  # Required for dynamically provisioned volumes
  adminID: admin
  adminKey: <admin key> 
...
```  

Применяем манифест:
```bash
$ k apply -f ceph-csi/secret.yaml 
secret/csi-cephfs-secret created
```  

Создаем Storageclass 
Создаем манифест [storageclass.yaml](./ceph-csi/storageclass.yaml)
```yaml
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: csi-cephfs-sc
provisioner: cephfs.csi.ceph.com
parameters:
  clusterID: <кластер ID>
  # CephFS filesystem name into which the volume shall be created
  fsName: cephfs
  # (optional) Ceph pool into which volume data shall be stored
  # pool: cephfs_data
  # (optional) Comma separated string of Ceph-fuse mount options.
  # For eg:
  # fuseMountOptions: debug
  # (optional) Comma separated string of Cephfs kernel mount options.
  # Check man mount.ceph for mount options. For eg:
  # kernelMountOptions: readdir_max_bytes=1048576,norbytes
  # The secrets have to contain user and/or Ceph admin credentials.
  csi.storage.k8s.io/provisioner-secret-name: csi-cephfs-secret
  csi.storage.k8s.io/provisioner-secret-namespace: ceph-csi-cephfs
  csi.storage.k8s.io/controller-expand-secret-name: csi-cephfs-secret
  csi.storage.k8s.io/controller-expand-secret-namespace: ceph-csi-cephfs
  csi.storage.k8s.io/node-stage-secret-name: csi-cephfs-secret
  csi.storage.k8s.io/node-stage-secret-namespace: ceph-csi-cephfs
  # (optional) The driver can use either ceph-fuse (fuse) or
  # ceph kernelclient (kernel).
  # If omitted, default volume mounter will be used - this is
  # determined by probing for ceph-fuse and mount.ceph
  # mounter: kernel
reclaimPolicy: Delete
allowVolumeExpansion: true
mountOptions:
  - debug
...

```

Применяем манифест: 
```bash
$ k apply -f ceph-csi/storageclass.yaml 
storageclass.storage.k8s.io/csi-cephfs-sc created
```  

Создаем PVC

Создаем [pvc.yaml](./ceph-csi/pvc.yaml)  

```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: csi-cephfs-pvc
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 15Gi
  storageClassName: csi-cephfs-sc
...

```
Применяем манифест: 
```bash
$ k apply -f ceph-csi/pvc.yaml 
persistentvolumeclaim/csi-cephfs-pvc created
```  

Проверка:
```bash
$ k get pvc csi-cephfs-pvc 
NAME             STATUS    VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS    AGE
csi-cephfs-pvc   Pending                                      csi-cephfs-sc   14s
```  
