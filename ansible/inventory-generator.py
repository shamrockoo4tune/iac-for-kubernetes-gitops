#!/usr/bin/python3
import json
import sys


try:
    if str(sys.argv[1]) in {'-h', '--help'}:
        print('''Usage: inventory-generator.py FILE
    Parses given FILE (i.e. result of $ terraform output -json > FILE) 
    and produce inventory.ini for ansible
    FILE to be passed as \$1 argument
    Purpose of inventory.ini is to describe nodes of ceph cluster with ceph-ansible
  
    - h, --help    display this message''')
        sys.exit(0)
    
    with open(str(sys.argv[1])) as file:
        data = json.load(file)
    
    hosts: dict = {} 
    for node in range(data['ceph_count']['value']):
        hosts[node] = {
            '': data['storage_nodes_hostnames']['value'][node],
            'ansible_host': data['storage_nodes_public_ips']['value'][node],
            'ansible_user': 'ansible',
            'ansible_private_key_file': '\'ansible_rsa\''
        }

    with open('inventory.ini', 'w') as inventory:
        inventory.write("[mons]\n")
        inventory.write('\n'.join([' '.join([k + '=' + v if k != '' else v for (k, v) in hosts[i].items()]) for i in range(len(hosts))])+'\n')                        
        inventory.write("[ods]\n")
        inventory.write('\n'.join([' '.join([k + '=' + v if k != '' else v for (k, v) in hosts[i].items()]) for i in range(len(hosts))])+'\n')   
    
except IndexError:
    print('''inventory-generator.sh: missing file operand
    For more information try:
    inventory-generator.sh --help''')
    sys.exit(1)
