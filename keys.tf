resource "tls_private_key" "ssh_key" {
  count     = var.ceph_count == 0 ? 0 : 1
  algorithm = "RSA"
  rsa_bits  = 4096

  provisioner "local-exec" {
    command = "sed -i \"s|ansible_rsa.pub|$key|\" cloud-init-ansible.yaml"
  }
}

resource "local_file" "public_key" {
  count    = var.ceph_count == 0 ? 0 : 1
  content  = tls_private_key.ssh_key[0].public_key_openssh
  filename = "./ansible/ansible_rsa.pub"
}

resource "local_sensitive_file" "private_key" {
  count                = var.ceph_count == 0 ? 0 : 1
  content              = tls_private_key.ssh_key[0].private_key_pem
  filename             = "./ansible/ansible_rsa"
  file_permission      = "0600"
  directory_permission = "0755"
}
