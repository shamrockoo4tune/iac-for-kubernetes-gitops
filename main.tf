locals {
  folder_id = "b1gjap7i9e06sdcbetb4"
}

module "yc-vpc" {
  source              = "github.com/sanyocc/terraform-yc-vpc.git"
  network_name        = "sgumerov-network"
  network_description = "Network created by terraform"
  private_subnets = [
    {
      name           = "subnet-1"
      zone           = var.zone
      v4_cidr_blocks = ["10.10.0.0/24"]
    }
  ]
}

module "yc-kubernetes" {
  source       = "github.com/terraform-yc-modules/terraform-yc-kubernetes.git"
  network_id   = module.yc-vpc.vpc_id
  cluster_name = "yc"
  description  = "Created by terraform for ShamrockOo4tune project"
  master_locations = [
    {
      zone      = "${module.yc-vpc.private_subnets["10.10.0.0/24"].zone}"
      subnet_id = "${module.yc-vpc.private_subnets["10.10.0.0/24"].subnet_id}"
    }
  ]
  node_groups_defaults = {
    name        = "node{{instance.index_in_zone}}-{{instance_group.name}}"
    platform_id = "standard-v3"
    # efk стэк прожорлив до ресурсов и на слабых конфигурациях не хочет стартовать
    node_cores                = 4
    node_memory               = 8
    node_gpus                 = 0
    core_fraction             = 100
    disk_type                 = "network-hdd"
    disk_size                 = 32
    preemptible               = false
    nat                       = false
    auto_repair               = true
    auto_upgrade              = false
    maintenance_day           = "monday"
    maintenance_start_time    = "20:00"
    maintenance_duration      = "3h30m"
    network_acceleration_type = "standard"
    container_runtime_type    = "containerd"
    ipv4                      = true
    ipv6                      = false
  }
  node_groups = {
    "default-pool" = {
      name        = "default-pool"
      description = "Kubernetes nodes default pool"
      fixed_scale = {
        size = 1
      }
      node_labels = {
        pool = "default"
      }
    }
    "infra-pool" = {
      name        = "infra-pool"
      description = "Kubernetes nodes infra pool"
      fixed_scale = {
        size = 3
      }
      node_labels = {
        pool = "infra"
      }
      node_taints = [
        "node-role=infra:NoSchedule"
      ]
    }
  }
}
