output "storage_nodes_hostnames" {
  value = yandex_compute_instance.ceph_cluster[*].hostname
}

output "storage_nodes_private_ips" {
  value = yandex_compute_instance.ceph_cluster[*].network_interface[0].ip_address
}

# output "ansible_host_public_ip" {
#   value = yandex_compute_instance.ansible_host.network_interface[0].nat_ip_address
# }

output "private_key" {
  value     = var.ceph_count == 0 ? null : tls_private_key.ssh_key[0].private_key_pem
  sensitive = true
}

output "public_key" {
  value     = var.ceph_count == 0 ? null : tls_private_key.ssh_key[0].public_key_openssh
  sensitive = true
}

output "ceph_count" {
  value = var.ceph_count
}
