resource "yandex_compute_disk" "hdd" {
  count     = var.ceph_count * 3 // x3 of ceph nodes, i.e. 3 disks per 1 node
  folder_id = "b1gjap7i9e06sdcbetb4"
  name      = "hdd-0${count.index + 1}"
  type      = "network-hdd"
  zone      = var.zone
  size      = 20
}

resource "yandex_compute_instance" "ceph_cluster" {
  count                     = var.ceph_count
  folder_id                 = "b1gjap7i9e06sdcbetb4"
  name                      = "storage-0${count.index + 1}"
  allow_stopping_for_update = true
  platform_id               = "standard-v2"
  zone                      = var.zone
  hostname                  = "ceph0${count.index + 1}-test"
  resources {
    cores         = 2
    memory        = 4 * 2 //each osd will need 4GiB, but tariff plan allows max 8Gib for 20% CPU instabce
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = 16
    }
  }

  secondary_disk {
    disk_id = yandex_compute_disk.hdd[(3 * count.index) + 0].id
  }

  secondary_disk {
    disk_id = yandex_compute_disk.hdd[(3 * count.index) + 1].id
  }

  secondary_disk {
    disk_id = yandex_compute_disk.hdd[(3 * count.index) + 2].id
  }

  network_interface {
    subnet_id  = module.yc-vpc.private_subnets["10.10.0.0/24"].subnet_id
    nat        = false
    ipv6       = false
    ip_address = "10.10.0.20${count.index + 1}"
  }

  metadata = {
    user-data : "#cloud-config\nusers:\n  - name: ansible\n    groups: sudo\n    shell: /bin/bash\n    sudo: ['ALL=(ALL) NOPASSWD:ALL']\n    ssh-authorized-keys:\n      - ${tls_private_key.ssh_key[0].public_key_openssh}"
  }
}
