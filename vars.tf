variable "zone" {
  type        = string
  default     = "ru-central1-a"
  description = "Идентификатор зоны доступности для размещения ресурса"
}

variable "image_id" {
  type        = string
  default     = "fd8nu8se2afcci22tpa5"
  description = "ID образа ОС. Debian11"
}

variable "ceph_count" {
  type        = number
  default     = 3
  description = "Количество нод Ceph. Скрипт сloud-init рассчитывает получить ровно 3 ноды. Но чтобы загасить кластер можно выставлять и в 0"
}
